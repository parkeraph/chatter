import React, {useState, useEffect} from 'react';
import { Container, Row, Col } from 'react-grid-system';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar, } from 'react-bootstrap'

import './App.css';

/*
message = {
  id: {int},         <-- incremental, non random 
  room: {int}, will be used later
  sender: usrID,
  content: {text},
  sentDate: {date},
} 
*/


function App() {
  const [newMessage, setNewMessage] = useState();
  const [messages, setMessages1] = useState([]);

  const effectHook = () => {
    
  }


  useEffect(effectHook, [])

  const handleSubmit = () => {
    //TODO
  }

  const handleInput = (event) => {
    setNewMessage(event.target.value);
    console.log(event.target.value);
  } 

  return (
  <div>
    <Navbar className='navbar'>
     
     </Navbar>

    <Container style={{width: '100%', margin: '0px'}}>
    <Row>
      <Col sm={0} md={2} lg={3} >
        
      </Col>
      <Col sm={12} md={8} lg={6}>
        <div className="chatBox">
            
        </div>
        <div className="inputBox">
          <form className="inputBox" onSubmit={handleSubmit}>
            <center><input type='text' onChange={handleInput} /><button type="submit">Send</button></center>
          </form>
        </div>
      </Col>
      <Col sm={0} md={2} lg={3}>
        
      </Col>
    </Row>
  </Container>
  </div>
  );
}

export default App;
