export const LANDING = "/";
export const HOME = "/home";
export const ROOM_LIST = "/rooms";
export const ROOM = "/ro/";
export const SIGNIN = "/signin";
export const SIGNUP = "/signup";