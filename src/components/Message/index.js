import React from './node_modules/react';

const SentMessage = ({sender, content}) => {
    return(
        <div className="message">
              <div className="messageContent">
                <span>{sender}</span>
                <span>{content}</span>
            </div> 
        </div>         
    )
}

const RecievedMessage = ({sender, content}) => {
    return(
        <div className="message message-recieved">
              <div className=" messageContent">
                <span>{sender}: </span>
                <span>{content}</span>
            </div> 
        </div>         
    )
}

export default RecievedMessage;
export {
    SentMessage
};
