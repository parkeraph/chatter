import React from 'react'
import {withFirebase} from '../Firebase'
import {useAuthState} from 'react-firebase-hooks/auth';
import {SessionContext} from '../Session'


const withSession = Component => {
    const SessionHof = (props) => {
        const fb = props.firebase
        const [userState, initState, errState] = useAuthState(fb.auth); 
        
        return(
            <SessionContext.Provider value={{userState, initState, errState}}>
                <Component {...{...props, userState, initState, errState}} />
            </SessionContext.Provider> 
        )
    }

    return withFirebase(SessionHof)
}

export default withSession;