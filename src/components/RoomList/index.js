import React, {useState} from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
import * as ROUTES from '../../constants/routes'

const Roomlist = (props) => {
    const rt = props.history;
    const [roomState, setRoomState] = useState([]);
    const getRooms = () => {
        axios.get("https://5d320b074901b4001401a67c.mockapi.io/chtr/rooms").then(res => {
            setRoomState(res.data);
            console.log(res.data);
        })
    }

    const roomListMap = roomState.map(itm => {
        return(
            <div key={itm.roomID}>
                <h3>{itm.roomName}</h3>
                <p>{itm.roomDescription}</p> 
                <button onClick={rt.push(ROUTES.ROOM+"")}></button>
            </div>
        )
    })

    useState(getRooms, []);
    return(
        <h1>ROOMLIST</h1>
    )
}

export default withRouter(Roomlist);