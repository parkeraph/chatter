import React from 'react';
import {BrowserRouter as Router, 
    Route} from 'react-router-dom';

import Landing from '../Landing';
import Roomlist from '../RoomList';
import Navigation from '../Navigation';
import {withSession} from '../Session';
import Signin from '../Signin';
import Signup from '../Signup';

import * as ROUTES from "../../constants/routes"

const App = () => {
    
    
        return(<Router>
            <Navigation />

            <Route exact path={ROUTES.LANDING} component={Landing}/>
            <Route path={ROUTES.ROOM_LIST} component={Roomlist} />
            <Route path={ROUTES.SIGNIN} component={Signin}/>
            <Route path={ROUTES.SIGNUP} component={Signup} />
            <Route path={ROUTES.ROOM} component={Room} />
        </Router>)
}

export default withSession(App);