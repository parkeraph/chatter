import React, { useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import {Button, Form, Container, Row, Col, Alert} from 'react-bootstrap';
import {withFirebase} from '../Firebase';

import * as ROUTES from '../../constants/routes';
import './index.css';

const SignUpPage = () => (
  <Container id="componentContainer">
    <Row>
        <Col></Col>
        <Col id="signUpContainer" xs = {12} sm = {10} md = {10} lg = {6}>
            <h1>SignUp</h1>
            <div id="formContainer">
                <SignUpForm />
            </div>
        </Col>
        <Col></Col>
    </Row>
  </Container>
);

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

const SignUpFormBase = (props) => {
    const fb = props.firebase
    const [formInputState, setFormInputState] = useState(INITIAL_STATE)
    
    const alert = () => {
        if (formInputState.error !== null){
            console.log("input error detected")
            return(
                <Alert id="alert" variant='danger'>{formInputState.error}</Alert>
            )
        }
    }

    const isError = 
        formInputState.passwordOne !== formInputState.passwordTwo ||
        formInputState.passwordOne === '' ||
        formInputState.email === '' ||
        formInputState.username === ''
    

     const handleSubmit = (event) => {
        
            fb.createUser(formInputState.email, formInputState.username, formInputState.passwordOne)
            .then(authUser => {
                setFormInputState({ ...INITIAL_STATE });
                props.history.push(ROUTES.LANDING);
            })
            .catch(error => {
                const stateObj = {
                    ...formInputState,
                    error: error.message,
                }

                setFormInputState(stateObj);
            });

            event.preventDefault();
     }

    const handleChange = (event) => {
        const valueType = event.target.id;
        const newValue = event.target.value;
        
        let formObj = {
            ...formInputState,
            [valueType]: newValue, 
        }

        if(formObj.passwordOne !== "" && formObj.passwordTwo !== ""){
            (formObj.passwordOne !== formObj.passwordTwo) ? formObj.error = "Passwords do not match" : formObj.error = null;
        }

        setFormInputState(formObj);
        console.log(formInputState)

    }

    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group >
                <Form.Label>Username</Form.Label>
                <Form.Control type="Username" id="username" placeholder="Enter a Username" onChange={handleChange}/>
            </Form.Group>

            <Form.Group >
                <Form.Label>Email address</Form.Label>
                <Form.Control type="Email" id="email" placeholder="example@example.com" onChange={handleChange}/>
            </Form.Group>

            <Form.Group >
                <Form.Label>Password</Form.Label>
                <Form.Control type="Password" id="passwordOne" placeholder="Password" onChange={handleChange}/>
            </Form.Group>
            
            <Form.Group >
                <Form.Label>Retype Password</Form.Label>
                <Form.Control type="Password" id="passwordTwo" placeholder="Retype Password" onChange={handleChange}/>
            </Form.Group>

            
            <Button disabled={isError} variant="primary" type="submit" id="submit button">
                Submit
            </Button>
            {alert()}

        </Form>

    )
}

const SignUpLink = () => (
  <p id="SignUpLink">
    Don't have an account? <Link to={ROUTES.SIGNUP}>Sign Up</Link>
  </p>
);

const SignUpForm = withRouter(withFirebase(SignUpFormBase))

export default SignUpPage;

export { SignUpForm, SignUpLink };