import React from "react"

const room = (props) => {
    const roomID = props.match.params.roomID;
    return(
        <div>
            <h1>ROOM {roomID}</h1>
        </div>
    )
}
