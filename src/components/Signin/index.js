import React, {useState} from 'react';
import { withRouter } from 'react-router-dom';
import {Button, Form, Container, Row, Col, Alert} from 'react-bootstrap';
import { withFirebase } from '../Firebase';

import './index.css'
import {SignUpLink} from '../Signup'
import * as ROUTES from '../../constants/routes';

const SignInPage = () => {
    return(
        <Container id="componentContainer">
        <Row>
            <Col></Col>
            <Col id="signInContainer" xs = {12} sm = {10} md = {10} lg = {6}>
                <h1>SignIn</h1>
                <div id="formContainer">
                    <SignInForm />
                </div>
            </Col>
            <Col></Col>
        </Row>
        </Container>
    )
}

const INIT_STATE = {
    email: '',
    password: '',
    error: null
}

const Signin = (props) => {
    const fb = props.firebase;
    const rt = props.history;
    const [formState, setFormState] = useState(INIT_STATE);

    const alert = () => {
        if (formState.error !== null && formState.error[0] === "auth"){
            return(
                <Alert id="alert" variant='danger'>Username or password is incorrect</Alert>
            )
        }else{return false}
    }

    const handleSubmit = (event) => {
        fb.signInWithEmailAndPassword(formState.email, formState.password).then(res => {
            console.log(res)
            setFormState(INIT_STATE);
            rt.push(ROUTES.LANDING);
        }).catch(err => {
            const errCode = err.code.split("/")
            const stateObj = {
                ...formState,
                error: errCode,
            }
            console.log(stateObj)
            setFormState(stateObj);
        })

        event.preventDefault();
    }

    const handleChange = (event) => {
        let stateObj = {
            ...formState,
            [event.target.id]: event.target.value,
        }

        setFormState(stateObj);
        console.log(formState);
    }

    return(
        <Form onSubmit={handleSubmit}>

        <Form.Group >
            <Form.Label>Email address</Form.Label>
            <Form.Control type="Email" id="email" placeholder="example@example.com" onChange={handleChange}/>
        </Form.Group>

        <Form.Group >
            <Form.Label>Password</Form.Label>
            <Form.Control type="Password" id="password" placeholder="Password" onChange={handleChange}/>
        </Form.Group>
        
        <Form.Row>
            <Button variant="primary" type="submit" id="submit button">
                Submit
            </Button>
            <SignUpLink id="SignInLink"/> 
        </Form.Row>
        {alert()}
    </Form>
    )
}

const SignInForm = withRouter(withFirebase(Signin));

export default SignInPage;