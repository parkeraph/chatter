import React from 'react';
import {Link} from 'react-router-dom';
import { Navbar, Nav, Button } from 'react-bootstrap'
import {withSession} from '../Session'

import SignOutButton from '../Signout'
import * as ROUTES from '../../constants/routes';

import './index.css'

const Nav_user = (props) => (
  <Navbar bg="dark" variant="dark">
        <Link to={ROUTES.LANDING}><Navbar.Brand>Chatter</Navbar.Brand></Link>
    <Nav className="mr-auto">
      <Nav.Link><Link to={ROUTES.HOME}>Home</Link></Nav.Link>
      <Nav.Link><Link to={ROUTES.ROOM_LIST}>Rooms</Link></Nav.Link>
      
    </Nav>
    <SignOutButton />
  </Navbar>
)

const Nav_default = () => (
  <Navbar bg="dark" variant="dark">
          <Link to={ROUTES.LANDING}><Navbar.Brand>Chatter</Navbar.Brand></Link>
          <Nav className="mr-auto">
            <Nav.Link><Link to={ROUTES.ROOM_LIST}>Rooms</Link></Nav.Link>
          </Nav>
    <Button variant="submit" value="SignIn"><Link to={ROUTES.SIGNIN}>SignIn</Link></Button>
    
  </Navbar>
)

const NAV_ADMIN = ( //future feature
  <Navbar bg="dark" variant="dark">
        <Link to={ROUTES.LANDING}><Navbar.Brand>Chatter <i>Admin</i></Navbar.Brand></Link>
    <Nav className="mr-auto">
      <Nav.Link><Link to={ROUTES.ROOM_LIST}>Rooms</Link></Nav.Link>
    </Nav>
    <SignOutButton />
  </Navbar>
)

const Navigation = (props) => {
  const session = props.userState;

  return session ? <Nav_user /> : <Nav_default />
};

export default withSession(Navigation);