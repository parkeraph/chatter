import app from "firebase";
import axios from "axios";

var FBconfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID
}

class Firebase {
    constructor(){
        app.initializeApp(FBconfig);
        this.auth = app.auth();
    }

    createUser = (email, username, password) => {
        return new Promise( (resolve, reject) => {
          this.auth.createUserWithEmailAndPassword(email, password).then(res => {
            axios.post("http://localhost:3001/api/users", {
              "uid": String(res.user.uid),
              "username": username 
            }).then(res => {
              resolve(res)
            }).catch(err => {
                //should handle the API failing, then delete the created user
                this.deleteUser(res.user).then(res => {
                  console.log(err);
                  reject(err)
                }).catch(delError => reject(err+" "+delError));
            });
          })
        })
     }

    signInWithEmailAndPassword = (email, password) => {
        return this.auth.signInWithEmailAndPassword(email, password);
     }

     signOut = () => {
       return this.auth.signOut();
     }
     doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

     doPasswordUpdate = password => {
       return this.auth.currentUser.updatePassword(password);
     }

     deleteUser = (thisUser) => {
       return thisUser.delete();
     }

}

export default Firebase;